(() => {
	const mobileWidth = 768;

	const addMenuBackground = () => {
		const pageWidth = window.innerWidth;
		const boddyOffset = document.body.scrollTop || document.documentElement.scrollTop;
		const navigation = document.querySelector("header nav"); 

		if(pageWidth > mobileWidth);
		boddyOffset > 0 ? navigation.classList.add("nav-fixed") : navigation.classList.remove("nav-fixed")
	}

	window.addEventListener("scroll", () => {
		addMenuBackground();
	})

	const onNavItemClick = () => {
		const navItemList = document.querySelectorAll(".section-link");
		const navItems = [...navItemList];

		navItems.forEach(item => {
			item.addEventListener("click", event => {
				event.preventDefault();

				const sectionId = event.target.getAttribute("href") || event.target.dataset.href;

				scrollToSection(sectionId);
				toggleNavigation()
			})
		})
	}


	const scrollToSection = sectionId => {
		let sectionPosition, sectionOffset;
		const navigationHeight = document.querySelector("header nav").offsetHeight;
		const pageWidth = window.innerWidth;

		if(sectionId !== "#") {
			sectionOffset = document.querySelector(sectionId).offsetTop;
			sectionPosition = pageWidth > mobileWidth ? sectionOffset - navigationHeight : sectionOffset;
		} else {
			sectionPosition = 0;
		}

		window.scrollTo({
			'behavior': 'smooth',
			'left': 0,
			'top': sectionPosition
		})
	}

	onNavItemClick();
})();


function toggleNavigation() {
	var navigation = document.getElementById('nav-toggle');
	navigation.classList.toggle('active');
}

